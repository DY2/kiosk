<?php
namespace system;

class App
{
    private $controller = 'MainController';
    private $method = 'actionIndex';
    private $params = [];

    public function __construct()
    {
        $url = $this->parseUrl();
        $this->params = $this->getParams();

        $pathController = __DIR__ . '/../controllers/' . ucfirst($url[1]) . 'Controller.php';

        if (isset($url[1])) {
            $this->controller = ucfirst($url[1]) . "Controller";
        }

        if (isset($url[2])) {
            $this->method = "action" . ucfirst($url[2]);
        }

        if (file_exists($pathController)) {
            spl_autoload_register(function ($class) {
                $class = str_replace('\\', '/', $class);
                require_once(__DIR__ . "/../{$class}.php");
            });

            $name = 'controllers\\' . $this->controller;
            //echo __DIR__ . "/../{$name}.php"; die();
            $this->controller = new $name();

            if (!method_exists($this->controller, $this->method)) {
                throw new \Exception('В текущем контроллере нет такого метода', 400);
            }

            call_user_func_array([$this->controller, $this->method], $this->params);
        }
    }

    private function parseUrl()
    {
        if (isset($_SERVER['REQUEST_URI'])) {
            $data = explode('?', $_SERVER['REQUEST_URI']);

            return isset($data[0]) ? explode('/', $data[0]) : [];
        }

        return [];
    }

    private function getParams()
    {
        return $_GET;
    }
}
