$(document).ready(function() {
    var isAmountPaid = false;
    var data = {
        nominal: {
            1: 0,
            2: 0,
            5: 0,
            10: 0
        },
        good: null
    };

    /**
     * Кликают по товару
     */
    $('.good').click(function() {
        let numberServing = 0;
        var obj = $(this);

        let number_servings = $('.serving_' + obj.attr('val')).text();
        number_servings = +number_servings;

        if (!isAmountPaid) {
            alert('Для начала внесите необходимую сумму...');
        } else if (!number_servings) {
            alert('Товар закончился');
        } else {
            let price = obj.attr('price');
            let good = obj.attr('name');
            let amount_paid = $('.amount_paid span').text();

            if ((+amount_paid) >= (+price)) {
                $('.amount_change span').text(+amount_paid - (+price));
                $('.change_good span').text(good);
                $('.change_good_amount span').text(price);
                $('.change_good, .change_good_amount').show();

                $('.good').removeClass('is_change_good');
                obj.addClass('is_change_good');

                data.good = obj.attr('val');

                $.ajax({
                    url: '/main/buy',
                    type: 'POST',
                    data: data,
                    dataType: "json",
                    success: function(response) {
                        if (response.success) {
                            let message = "Спасибо за Вашу покупку.\nВаш \"" + good + "\" готов.\n";

                            let change = (+amount_paid) - (+price);
                            if (change) {
                                message += "Сдача " + change + " рублей будет автоматически возвращена на кошелек.\n";
                            }

                            message += "Страница сейчас будет автоматически перезагружена";

                            alert(message);
                        } else {
                            alert(response.message);
                        }

                        window.location.reload();
                    }
                });
            } else {
                alert('Недостаточно средств, внесите необходимую сумму...');
            }
        }
    });

    /**
     * Кликают по номиналу валюты
     */
    $('.nominal_money li').click(function() {
        isAmountPaid = true;

        var obj = $(this);
        let nominalValue = obj.attr('val');

        $('.screen_text').hide();
        $('.amount_change, .amount_paid').show();

        let wallet_coin_number_obj = $('.wallet_coins li[coin="' + nominalValue + '"] span.wallet_number');
        let wallet_coin_number = wallet_coin_number_obj.text();
        wallet_coin_number = (+wallet_coin_number) - 1;

        if (wallet_coin_number < 0) {
            alert('В Вашем кошельке монеты с текущим номиналом закончились');
        } else {
            wallet_coin_number_obj.text(wallet_coin_number);

            data.nominal[nominalValue] += 1;

            let amount_paid = $('.amount_paid span').text();
            amount_paid = (+amount_paid) + (+nominalValue);

            $('.amount_paid span').text(amount_paid);
            let change_good_amount = $('.change_good_amount span').text();

            if (amount_paid > (+change_good_amount)) {
                $('.amount_change span').text(amount_paid - (+change_good_amount));
            }

            let wallet_balance = $('.wallet_balance span').text();
            $('.wallet_balance span').text(+wallet_balance - (+nominalValue));
        }
    });

    /**
     * Возвращаем деньги / Сдачу
     */
    $('.get_change').click(function() {
        let amount = 0;

        $.each(data.nominal, function(coin, number) {
            amount += coin * number;

            let wallet_coin_number_obj = $('.wallet_coins li[coin="' + coin + '"] span.wallet_number');
            let wallet_coin_number = wallet_coin_number_obj.text();
            wallet_coin_number_obj.text(+wallet_coin_number + number);

            data.nominal[coin] = 0;
        });

        let wallet_balance = $('.wallet_balance span').text();
        $('.wallet_balance span').text(+wallet_balance + amount);

        $('.screen_text').show();
        $('.amount_change, .amount_paid, .change_good, .change_good_amount').hide();
        $('.amount_change span, .amount_paid span').text(0);
    });
});