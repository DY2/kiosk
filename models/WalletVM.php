<?php
namespace models;

use system\Model;

/**
 * Class WalletVM
 * @package models
 *
 * @var string $name
 * @var string $coins
 * @var double $balance
 */
class WalletVM extends Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wallet_vm';
    }

    public function getOne($sql)
    {
        $result = parent::getOne($sql);

        if ($result['coins']) {
            $result['coins'] = json_decode($result['coins']);
        }

        return $result;
    }
}
