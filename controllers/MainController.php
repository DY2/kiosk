<?php
namespace controllers;

use system\Controller;

class MainController extends Controller
{
    public function actionIndex()
    {
        /**
         * @var $modelGoods \models\Goods
         */
        $modelGoods = $this->model('Goods');
        $resultGoods = $modelGoods->getAll("SELECT * FROM {$modelGoods::tableName()}");

        /**
         * @var $modelWallet \models\Wallet
         */
        $modelWallet = $this->model('Wallet');
        $walletInfo = $modelWallet->getOne("SELECT * FROM {$modelWallet::tableName()}");

        /**
         * @var $modelWalletVM \models\WalletVM
         */
        $modelWalletVM = $this->model('WalletVM');
        $walletInfoVM = $modelWalletVM->getOne("SELECT * FROM {$modelWalletVM::tableName()}");

        $this->renderView('main/index', [
            'resultGoods' => $resultGoods,
            'walletInfo' => $walletInfo,
            'walletInfoVM' => $walletInfoVM
        ]);
    }

    public function actionBuy()
    {
        $goodId = $_POST['good'];
        $nominals = $_POST['nominal'];

        if (!is_numeric($goodId) || !is_array($nominals)) {
            $answer = [
                'success' => false,
                'message' => 'Неверный формат данных'
            ];
        } else {
            $goodId = (int)$goodId;

            $answer = [
                'success' => false,
                'message' => null
            ];

            /**
             * @var $modelGood \models\Goods
             */
            $modelGood = $this->model('Goods');
            $resultGood = $modelGood->getOne("SELECT * FROM {$modelGood::tableName()} WHERE id = {$goodId}");

            if ($resultGood) {
                if (!$resultGood['number']) {
                    $answer['message'] = 'Товар закончился';
                }

                $calculate = $this->calculateChange($nominals, (int)$resultGood['price']);

                $callProcedure = $modelGood->call("CALL good_buy(:goodId, :pay, :change)", [
                    ':goodId' => $goodId,
                    ':pay' => json_encode($calculate['pay']),
                    ':change' => json_encode($calculate['change'])
                ]);

                if ($callProcedure) {
                    $answer['success'] = true;
                } else {
                    $answer['message'] = 'Произошла ошибка при обработке данных';
                }
            } else {
                $answer['message'] = 'Не найден необходимый товар';
            }
        }

        echo json_encode($answer);
    }

    /**
     * @param array $nominals
     * @param int $price
     * @return array|bool
     *
     * P.S. Плохо, что пробегаюсь 2 раза по одному и тому же массиву $nominals, делал на скорую руку
     */
    public function calculateChange(array $nominals, int $price)
    {
        if (!$nominals) {
            return false;
        }

        krsort($nominals);

        $payCoins = $changeCoins = [];
        $nominalSum = 0;

        foreach ($nominals as $coin => $number) {
            if (!$number) {
                continue;
            }

            for ($i = 1; $i <= $number; $i++) {
                $nominalSum += $coin;

                if (isset($payCoins[$coin])) {
                    $payCoins[$coin] += 1;
                } else {
                    $payCoins[$coin] = 1;
                }

                if ($nominalSum >= $price) {
                    break(2);
                }
            }
        }

        $change = $nominalSum - $price;

        foreach ($nominals as $coin => $number) {
            if ($coin > $change) {
                continue;
            }

            if ($change % $coin == 0) {
                $countCurrentNominal = $change / $coin;
            } else {
                $countCurrentNominal = floor($change / $coin);
            }

            $changeCoins[$coin] = $countCurrentNominal;
            $change -= $coin * $countCurrentNominal;

            if (!$change) {
                break;
            }
        }

        return [
            'pay' => $payCoins,
            'change' => $changeCoins
        ];
    }
}
