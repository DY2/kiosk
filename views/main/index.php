<?php
/**
 * @var array $data
 */
?>

<div class="info">
    <div class="screen">
        <p class="screen_text">Внесите необходимую сумму...</p>
        <p class="change_good">Выбран: <span></span></p>
        <p class="change_good_amount">Сумма: <span>0</span> руб.</p>
        <p class="amount_paid">Внесенная сумма: <span>0</span> руб.</p>
        <p class="amount_change">Сдача: <span>0</span> руб.</p>
    </div>

    <div class="clear"></div>

    <div class="common_block">
        <div class="block_goods">
            <?php if (isset($data['resultGoods'])) : ?>
                <?php foreach ($data['resultGoods'] as $key => $good) : ?>
                    <div class="good" name="<?= $good['name'] ?>" price="<?= $good['price'] ?>" val="<?= $good['id'] ?>">
                        <p><?= $good['name'] ?><br>(<?= $good['price'] ?> руб.)</p>
                        <img src="/web/images/<?= $good['image'] ?>" width="70px"/>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>

        <div class="block_other">
            <div class="deposit_money">
                <p>Внесите деньги, нажав на соотв. монету(ы):</p>

                <ul class="nominal_money">
                    <li val="1">1</li>
                    <li val="2">2</li>
                    <li val="5">5</li>
                    <li val="10">10</li>
                </ul>
            </div>


            <p class="get_change">Получить сдачу</p>
        </div>
    </div>
</div>

<div class="other_info">
    <div class="wallet_info_block">
        <p><b><?= $data['walletInfo']['name'] ?></b></p>
        <p class="wallet_balance">Баланс: <span><?= $data['walletInfo']['balance'] ?></span> руб.</p>

        <div class="wallet_coins">
            <ul>
                <?php foreach ($data['walletInfo']['coins'] as $coin => $number) { ?>
                    <li coin="<?= $coin ?>"">
                        <span class="wallet_coin"><?= $coin ?></span> -
                        <span class="wallet_number"><?= $number ?></span> шт.
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="number_servings">
        <b>Остаток товара:</b>

        <ul>
            <?php if (isset($data['resultGoods'])) {
                foreach ($data['resultGoods'] as $good) { ?>
                    <li>
                        <span><?= $good['name'] ?></span> (<span class="serving_<?= $good['id'] ?>"><?= $good['number'] ?></span> порций)
                    </li>
                <?php }
            } ?>
        </ul>
    </div>

    <div class="wallet_vm_info_block">
        <p><b><?= $data['walletInfoVM']['name'] ?></b></p>
        <p>Баланс: <?= $data['walletInfoVM']['balance'] ?> руб.</p>

        <div class="wallet_vm_coins">
            <ul>
                <?php foreach ($data['walletInfoVM']['coins'] as $coin => $number) { ?>
                    <li>
                        <span><?= $coin ?></span> -
                        <span><?= $number ?></span> шт.
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>