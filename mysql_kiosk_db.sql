/*
 Navicat Premium Data Transfer

 Source Server         : db4free.net
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : db4free.net:3306
 Source Schema         : mysql_kiosk_db

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : 65001

 Date: 22/05/2018 15:03:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(127) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `number` int(5) NOT NULL,
  `price` double(5, 0) NOT NULL,
  `image` varchar(127) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES (1, 'Чай', 10, 13, '2.png');
INSERT INTO `goods` VALUES (2, 'Кофе', 20, 18, '3.png');
INSERT INTO `goods` VALUES (3, 'Кофе с молоком', 20, 21, '4.png');
INSERT INTO `goods` VALUES (4, 'Сок', 15, 35, '1.png');

-- ----------------------------
-- Table structure for wallet
-- ----------------------------
DROP TABLE IF EXISTS `wallet`;
CREATE TABLE `wallet`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(127) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `coins` json NOT NULL,
  `balance` double(5, 0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wallet
-- ----------------------------
INSERT INTO `wallet` VALUES (1, 'Кошелек пользователя', '{\"1\": 10, \"2\": 30, \"5\": 20, \"10\": 15}', 320);

-- ----------------------------
-- Table structure for wallet_vm
-- ----------------------------
DROP TABLE IF EXISTS `wallet_vm`;
CREATE TABLE `wallet_vm`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(127) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `coins` json NOT NULL,
  `balance` double(5, 0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wallet_vm
-- ----------------------------
INSERT INTO `wallet_vm` VALUES (1, 'Кошелек VM', '{\"1\": 100, \"2\": 100, \"5\": 100, \"10\": 100}', 1800);

-- ----------------------------
-- Procedure structure for good_buy
-- ----------------------------
DROP PROCEDURE IF EXISTS `good_buy`;
delimiter ;;
CREATE PROCEDURE `good_buy`(IN goodId INT, IN pay JSON, IN changes JSON)
BEGIN
		DECLARE errno INT;
		DECLARE msg TEXT;
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
			BEGIN
				GET STACKED DIAGNOSTICS CONDITION 1 errno = MYSQL_ERRNO, msg = MESSAGE_TEXT;
				SELECT FALSE AS 'result', errno AS MYSQL_ERRNO, msg AS MESSAGE_TEXT;
				ROLLBACK;
			END;

  START TRANSACTION;
		
	SET @g_price = 0;
	SELECT price INTO @g_price FROM goods WHERE id = goodId;
	
	SET @pay10 = IFNULL(JSON_EXTRACT(pay, '$[0]."10"'), 0);
	SET @pay5 = IFNULL(JSON_EXTRACT(pay, '$[0]."5"'), 0);
	SET @pay2 = IFNULL(JSON_EXTRACT(pay, '$[0]."2"'), 0);
	SET @pay1 = IFNULL(JSON_EXTRACT(pay, '$[0]."1"'), 0);
	
	SET @diff10 = @pay10 - IFNULL(JSON_EXTRACT(changes, '$[0]."10"'), 0);
	SET @diff5 = @pay5 - IFNULL(JSON_EXTRACT(changes, '$[0]."5"'), 0);
	SET @diff2 = @pay2 - IFNULL(JSON_EXTRACT(changes, '$[0]."2"'), 0);
	SET @diff1 = @pay1 - IFNULL(JSON_EXTRACT(changes, '$[0]."1"'), 0);

	UPDATE wallet SET 
		coins = JSON_SET(coins, '$[0]."10"', (coins->'$[0]."10"') - @diff10),
		coins = JSON_SET(coins, '$[0]."5"', (coins->'$[0]."5"') - @diff5),
		coins = JSON_SET(coins, '$[0]."2"', (coins->'$[0]."2"') - @diff2),
		coins = JSON_SET(coins, '$[0]."1"', (coins->'$[0]."1"') - @diff1),
		balance = balance - @g_price
	WHERE id = 1;
	
	UPDATE wallet_vm SET 
		coins = JSON_SET(coins, '$[0]."10"', (coins->'$[0]."10"') + @diff10),
		coins = JSON_SET(coins, '$[0]."5"', (coins->'$[0]."5"') + @diff5),
		coins = JSON_SET(coins, '$[0]."2"', (coins->'$[0]."2"') +  @diff2),
		coins = JSON_SET(coins, '$[0]."1"', (coins->'$[0]."1"') +  @diff1),
		balance = balance + @g_price
	WHERE id = 1;

	UPDATE goods SET number = number - 1 WHERE id = goodId;
	
	SELECT TRUE AS 'result';
	
	COMMIT;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
