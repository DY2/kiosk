<?php
namespace models;

use system\Model;

/**
 * Class Goods
 * @package models
 *
 * @var string $name
 * @var integer $number
 * @var double $price
 */
class Goods extends Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods';
    }
}
