<?php
namespace system;

class Controller
{
    public function model($modelName)
    {
        $modelName = str_replace('\\', '/', $modelName);
        require_once(__DIR__ . "/../models/{$modelName}.php");

        $modelName = "\\models\\" . $modelName;
        return new $modelName();
    }

    public function renderView($name, $data = [])
    {
        require_once(__DIR__ . "/../views/layout_begin.php");
        require_once(__DIR__ . "/../views/{$name}.php");
        require_once(__DIR__ . "/../views/layout_end.php");
    }
}
