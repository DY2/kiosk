<?php
namespace system;


class Model extends Db
{
    public function getOne($sql)
    {
        $stmt = self::$connect->prepare($sql);

        if ($stmt->execute()) {
            return $stmt->fetch(\PDO::FETCH_ASSOC);
        }

        return [];
    }

    public function getAll($sql)
    {
        $stmt = self::$connect->prepare($sql);
        if ($stmt->execute()) {
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }

        return [];
    }

    public function call($sql, $params)
    {
        $stmt = self::$connect->prepare($sql);

        return $stmt->execute($params);
    }
}
