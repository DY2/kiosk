<?php
namespace models;

use system\Model;

/**
 * Class Wallet
 * @package models
 *
 * @var integer $id
 * @var string $name
 * @var string $coins
 * @var double $balance
 */
class Wallet extends Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wallet';
    }

    public function getOne($sql)
    {
        $result = parent::getOne($sql);

        if ($result['coins']) {
            $result['coins'] = json_decode($result['coins']);
        }

        return $result;
    }
}
