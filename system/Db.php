<?php
namespace system;

class Db
{
    private $host;
    private $username;
    private $password;
    private $dbname;

    protected static $connect;

    public function __construct()
    {
        date_default_timezone_set('Europe/Moscow');

        $settings = self::getSettings();

        $this->host = $settings['host'] ?? null;
        $this->username = $settings['username'] ?? null;
        $this->password = $settings['password'] ?? null;
        $this->dbname = $settings['dbname'] ?? null;

        self::$connect = $this->connect();
    }

    private static function getSettings()
    {
        $settings = [
            'dev' => [
                'host' => '10.0.2.2',
                'username' => 'kiosk_user',
                'password' => 'kiosk_user',
                'dbname' => 'kiosk'
            ],
            'prod' => [
                'host' => 'db4free.net',
                'username' => 'mysql_kiosk_user',
                'password' => 'mysql_kiosk_user',
                'dbname' => 'mysql_kiosk_db'
            ]
        ];

        return $settings[ENV] ?? $settings['dev'];
    }

    private function connect()
    {
        try {
            return new \PDO("mysql:dbname=$this->dbname;host=$this->host", $this->username, $this->password);
        } catch (\Exception $e) {
            if (ENV == 'dev') {
                print_r($e->getMessage());
                die();
            }

            return null;
        }
    }
}
